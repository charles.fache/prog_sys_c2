/* 
    Here we define the data structure which holds the commands to send, and some other related datas 
*/

use std::sync::{Arc, Mutex};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Command {Nothing, ListDir, AddFile}

pub struct CommandToSend {
    cmd: Command,
    read_count: u16
}

impl CommandToSend{
    pub fn new() -> CommandToSend {
        CommandToSend { cmd: Command::Nothing, read_count: 0}
    }

    pub fn snew() -> ProtectedCommandToSend {
        Arc::new(Mutex::new(CommandToSend::new()))
    }

    pub fn read(&mut self) -> Command{
        let ret: Command = self.cmd;

        self.read_count += 1;
        
        ret
    }

    pub fn write(&mut self, inp: Command){
        self.cmd = inp;

        self.read_count = 0;
    }

    pub fn get(&self) -> (String, u16){
        (self.cmd.to_str().to_string(), self.read_count)
    }
}

impl Command{
    fn to_str(&self) -> &str{
        match self {
            Command::Nothing => "None",
            Command::AddFile => "touch",
            Command::ListDir => "ls"
        }
    }   
}

pub type ProtectedCommandToSend = Arc<Mutex<CommandToSend>>;