/*
    Commander is the CLI where you can control the command sent
 */


use super::data::{Command, ProtectedCommandToSend};

pub fn commander_main(shared_cmd: ProtectedCommandToSend){
    loop{
        let cmd: Command = str_to_cmd(user_input());
        
        if cmd == Command::Nothing {
            println!("Unknown Command !\nImplemented : touch, ls");
        } else {
            let mut cts = shared_cmd.lock().unwrap();
            let formatted_prev: (String, u16) = cts.get();
            println!("Previous command {} was executed {} times", formatted_prev.0, formatted_prev.1);

            cts.write(cmd);
            std::mem::drop(cts);

        }
    }
}

fn user_input() -> String{
    use std::io::{stdin, stdout, Write};
    let mut rep: String = String::new();

    print!("> ");
    let _=stdout().flush();
    stdin().read_line(&mut rep).unwrap();

    rep.as_str().trim().to_string() //Use the str.trim 
} 

fn str_to_cmd(input: String) -> Command{
    match input.as_str(){
        "touch" => Command::AddFile,
        "ls" => Command::ListDir,
        _ => Command::Nothing
    }
}