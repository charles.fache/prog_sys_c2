/*
    This is the entry point. It must start shared data structure and threads
 */

mod data;
mod commander;
mod responder;

use std::sync::{Arc};
use std::thread;

use data::{CommandToSend, ProtectedCommandToSend};

fn main() {
    let shared_cmd: ProtectedCommandToSend = CommandToSend::snew();

    let commander_shared_cmd = shared_cmd.clone();
    thread::spawn(move ||{
        commander::commander_main(commander_shared_cmd);
    });

    responder::responder_main(shared_cmd.clone()).expect("Responder crashed");
}